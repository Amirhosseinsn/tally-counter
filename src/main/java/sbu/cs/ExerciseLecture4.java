package sbu.cs;

import java.util.*; 
import java.util.concurrent.LinkedBlockingQueue; 

public class ExerciseLecture4
{
	public long factorial(int n)
	{
		long fact = 1;
		for (int i = 1 ; i <= n ; i++)
			fact *= i;

		return fact;
	}

	public long fibonacci(int n)
	{
		if (n == 1 || n == 2)
			return 1;

		long n1 = 1;
		long n2 = 1;
		long nth = 0;
		for (int i = 2 ; i < n ; i++)
		{
			nth = n2 + n1;
			n1 = n2;
			n2 = nth;
		}

		return nth;
	}

	public String reverse(String word)
	{
		StringBuilder tempStr = new StringBuilder(word);
		StringBuilder tempReverseStr = tempStr.reverse();
		String reverseStr = tempReverseStr.toString();
		return reverseStr;
	}

	public boolean isPalindrome(String line)
	{
		line = line.replaceAll("\\s", ""); 
		for (int i = 0 ; i < line.length() / 2 ; i++)
		{
			if (line.charAt(i) != line.charAt(line.length() - 1 - i))
				return false;
		}

		return true;
	}

	public char[][] dotPlot(String str1, String str2)
	{
		char[][] map = new char[str1.length() + 1][str2.length() + 1];
		map[0][0] = ' ';

		for (int i = 1 ; i < str1.length() + 1 ; i++)
			map[i][0] = str1.charAt(i - 1);
		for (int j = 1 ; j < str2.length() + 1 ; j++)
			map[0][j] = str2.charAt(j - 1);

		for (int i = 1 ; i < str1.length() + 1 ; i++)
		{
			for (int j = 1 ; j < str2.length() + 1 ; j++)
			{
				if (map[i][0] == map[0][j])
					map[i][j] = '*';
				else
					map[i][j] = ' ';
			}
		}

		return map;
	}
}