package sbu.cs;

import java.lang.Math;
import java.io.IOException;
import java.util.Random;
import java.util.*; 
import java.util.concurrent.LinkedBlockingQueue; 

public class ExerciseLecture5
{
	public static String weakPassword(int length)
	{
		String password = "";
		Random rand = new Random();
		for (int i = 0 ; i < length ; i++)
		{
			int randNum = rand.nextInt(26);
			password += (char)(randNum + (int)('a'));
		}
		return password;
	}

	public String strongPassword(int length)
	{
		if (length <= 2)
			throw new ArithmeticException("The length of the password must be more than two.");

		String password = weakPassword(length);
		Random rand = new Random();

		int digitPlace = rand.nextInt(length);
		int specialCharacterPlace;
		do
		{
			specialCharacterPlace = rand.nextInt(length);
		}
		while (digitPlace == specialCharacterPlace);

		char randDig = (char)(rand.nextInt('9' - '0') + '0');
		char randChar = (char)(rand.nextInt('/' - '!') + '/');

		password = password.substring(0 , digitPlace - 1) + randDig + password.substring(digitPlace + 1 , length - 1);
		password = password.substring(0 , specialCharacterPlace - 1) + randChar + password.substring(specialCharacterPlace + 1 , length - 1);

		return password;
	}

	//using Binet's formula.
	public boolean isFiboBin(int n)
	{
		double a = (1 + Math.sqrt(5)) / 2;
		double b = (1 - Math.sqrt(5)) / 2;
		int nthFib;
		int nthFibBin = 0;
		int num = 0;
		for (int i = 1 ; num <= n ; i++)
		{
			nthFib = (int)((Math.pow(a , i) - Math.pow(b , i)) / (a - b));
			nthFibBin = 0;
			for (int j = 0 ; nthFib <= n ; j++)
			{
				if (nthFib >= Math.pow(2 , j))
				{
					nthFibBin++;
					nthFib -= Math.pow(2 , j);
				}
			}
			num = nthFib + nthFibBin;
			if (num == n)
				return true;
		}

		return false;
	}
}