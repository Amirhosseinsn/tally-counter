package sbu.cs;

import java.util.List;
import java.io.IOException;

public class ExerciseLecture6
{
	public long calculateEvenSum(int[] arr)
	{
		long sum = 0;
		for (int i = 0 ; i < arr.length ; i+=2)
			sum += arr[i];

		return sum;
	}

	public int[] reverseArray(int[] arr)
	{
		int temp;
		for (int i = 0 ; i < arr.length / 2 ; i++)
		{
			temp = arr[arr.length - 1 - i];
			arr[arr.length - 1 - i] = arr[i];
			arr[i] = temp;
		}

		return arr;
	}

	public double[][] matrixProduct(double m1[][] , double m2[][])
	{
		if (m1[0].length != m2.length)
			throw new RuntimeException("These two matrixes can't be producted.");

		double product[][] = new double[m1.length][m2[0].length];

		for(int i = 0 ; i < m1.length ; i++)
		{
			for (int j = 0 ; j < m2[0].length ; j++)
			{
				for (int k = 0 ; k < m2.length ; k++)
					product[i][j] += m1[i][k] * m2[k][j];
			}
		}
		return product;
	}

	/*
	 *   implement a function that return array list of array list of string
	 *   from a 2-dim string array
	 *   lecture 6 page 30
	 */
	public List<List<String>> arrayToList(String[][] names)
	{
		return null;
	}

	/*
	 *   implement a function that return a list of prime factor of integer n
	 *   in ascending order
	 *   lecture 6 page 30
	 */
	public List<Integer> primeFactors(int n)
	{
		return null;
	}

	/*
	 *   implement a function that return a list of words in a given string
	 *   lecture 6 page 30
	 */
	public List<String> extractWord(String line)
	{
		return null;
	}
}